from pydub import AudioSegment

sound = AudioSegment.from_file("audios_laika/si.mp3")
sound = sound.set_frame_rate(8000)
sound = sound.set_channels(1)
sound.export("audios_laika/si.wav", format="wav")